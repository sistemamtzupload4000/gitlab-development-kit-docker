FROM ubuntu:artful

RUN apt-get update -y && \
    apt-get install -y software-properties-common python-software-properties \
      git postgresql postgresql-contrib libpq-dev redis-server libicu-dev cmake \
      build-essential nodejs npm libre2-dev libkrb5-dev golang-1.8-go ed pkg-config \
      ruby-dev

RUN npm install phantomjs-prebuilt@2.1.12 yarn -g --unsafe-perm
RUN gem install gitlab-development-kit

EXPOSE 3000
