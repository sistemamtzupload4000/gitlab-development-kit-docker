## GitLab Development Kit meets Docker

This repository contains minimal set of scripts to create a virtual environment for GitLab Development using Docker.

### Requirements

1. Docker Engine,
2. Make installed.

### Start

```bash
$ make
make start
make[1]: Entering directory '/home/ayufan/Sources/gdk'
Building environment...
sha256:c84dfaba82a9b46d888d4add11f180ce54dec80d035f0fa027d4797322282c2a
Starting workspace...
make[1]: Leaving directory '/home/ayufan/Sources/gdk'
Entering shell...
ayufan@gitlab-development-kit:~/Sources/gdk$ gdk init
ayufan@gitlab-development-kit:~/Sources/gdk$ cd gitlab-development-kit
ayufan@gitlab-development-kit:~/Sources/gdk$ gdk init
```

### Stop

```bash
make stop
```

It will remove Docker Container, but it will not remove the data.

## Author

Kamil Trzciński, 2017, GitLab

## License

MIT
