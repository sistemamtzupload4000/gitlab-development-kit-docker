all: shell

build:
	@echo Building environment...
	@docker build -q -t gitlab-development-kit .

start: build
	@echo Starting workspace...
	@docker run --name=gdk \
		--rm \
		-d -i \
		-e HOME -v $(HOME):$(HOME) \
		-e USER -u $$(id -u):$$(id -g) \
		-v /etc/passwd:/etc/passwd:ro \
		-h gitlab-development-kit \
		-v $(CURDIR):$(CURDIR) \
		-w $(CURDIR) \
		-p 3000:3000 \
		gitlab-development-kit \
		cat

ensure_started:
	-make start

shell: ensure_started
	@echo Entering shell...
	@docker exec -it gdk /bin/bash

stop:
	@echo Removing current environment...
	-docker rm -f gdk
